# Software Huellas Nucleares



## Getting started

Software for nuclear track estimation, developed for the Peruvian Institute of Nuclear Energy. Utilizes artificial vision recognition to identify edges through Canny algorithms

## Clone repository

```
git clone https://gitlab.com/cancertataje/software-huellas-nucleares.git
```

## Install dependencies

```
pip install -r requirements.txt
```

## Authors and acknowledgment
MSc. Paolo G. Tataje

## License
Free

