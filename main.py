from PyQt5 import QtCore, QtGui, QtWidgets, uic
from PyQt5.QtGui import QIcon, QPixmap, QImage
from PyQt5.QtCore import QTimer, Qt
from PyQt5.QtWidgets import (QFileDialog, QApplication, QMainWindow,
                             QLineEdit, QPushButton, QLabel, QMessageBox)

import numpy
import cv2
from PIL import Image, ImageTk
import shutil
import tifffile


class Ui_Proceso(QMainWindow):

    def __init__(self):        
        super(Ui_Proceso, self).__init__()
        uic.loadUi("Ingreso.ui", self) 

        self.setWindowIcon(QIcon("0.ico"))

        self.InsertarBtn.clicked.connect(self.SelecImag)
        self.NextBtn.clicked.connect(self.NextImag)
        self.BackBtn.clicked.connect(self.BeforeImag)

        self.show()
    
    def BeforeImag(self):
        try:
            Initial = int(self.label_4.text())

            if (Initial-2 > 0 or Initial-2 == 0):

                Seleccion = self.process(self.files[Initial-2])
                self.lineEdit_2.setText(str(Seleccion))

                self.label_4.setText(str(Initial-1))

        except Exception as e:
            print(e)

    def NextImag(self):
        try:
            Initial = int(self.label_4.text())

            Seleccion = self.process(self.files[Initial])
            self.lineEdit_2.setText(str(Seleccion))

            self.label_4.setText(str(Initial+1))

        except Exception as e:
            print(e)
    
    def SelecImag(self):
        try:
            filenameT = QFileDialog.getOpenFileNames(None, 'Abrir Imagen', filter='CR2 (*.cr2);;PNG (*.png);;JPEG (*.jpg;*.jpeg;*.jpe;*.jfif)')
                        
            filename = filenameT[0]
            self.files = filename
            Cant_Imag = len(filename)
            self.label_5.setText(str(Cant_Imag))

            self.label_4.setText("1")

            
            Seleccion = self.process(filename[0])

            self.lineEdit_2.setText(str(Seleccion))

            index = 0
            for i in range(0, Cant_Imag):
                Val = self.process(filename[i])
                index = index + Val
            
            Promedio = index / Cant_Imag

            self.lineEdit_1.setText(str(round(Promedio)))

        except Exception as e:
            print(e)
    
    
    def process(self, frame):
        try:            
            image = tifffile.imread(frame)
            
            if image is not None:        
                pro_image = cv2.cvtColor(image, cv2.COLOR_RGB2BGR)
                gray = cv2.cvtColor(pro_image, cv2.COLOR_BGR2GRAY)            
                blurred = cv2.GaussianBlur(gray, (5, 5), 0)                
                edges = cv2.Canny(blurred, 50, 120)
                
                contours, _ = cv2.findContours(edges.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)                
                cv2.drawContours(pro_image, contours, -1, (255, 0, 0), 15)
                

                q_image = QImage(pro_image, pro_image.shape[1], pro_image.shape[0], QImage.Format_RGB888).rgbSwapped()
                pixmap = QPixmap.fromImage(q_image)
                self.label_Imag.setPixmap(pixmap.scaled(self.label_Imag.size(), Qt.KeepAspectRatio, Qt.SmoothTransformation))                

                Val = len(contours)
            
            else:
                Val = 0
            
            return Val

        except Exception as e:
            print(e)



if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    app.setWindowIcon(QtGui.QIcon('0.ico'))
    window = Ui_Proceso()         
    window.show()    
    sys.exit(app.exec_())
